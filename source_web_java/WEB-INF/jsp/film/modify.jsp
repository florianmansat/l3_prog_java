<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import ="model.*" %>
<html>
    <body>
        <%
            if (request.getAttribute("film") != null) {
                Film film = (Film) request.getAttribute("film");
        %>
        <form action='updateFilm' method='post'>
            <input type='hidden' name='idFilm' id='idFilm' value='<%=film.getId()%>'>
            <div>
                <label for='name'>Titre :</label>
                <input type='text' name='titre' id='titre' value='<%=film.getTitre()%>' required>
            </div>
            <div>
                <label for='name'>Date de sortie :</label>
                <input type='number' name='dateDeSortie' id='dateDeSortie' value='<%=film.getDateDeSortie()%>' required>
            </div>
            <div>
                <label for='name'>Numéro de l'épisode :</label>
                <input type='number' name='numero' id='numero' value='<%=film.getNumeroEpisode()%>' required>
            </div>
            <div>
                <label for='name'>Coût :</label>
                <input type='number' name='cout' id='cout' value='<%=film.getCout()%>' required>
            </div>
            <div>
                <label for='name'>Recette :</label>
                <input type='number' name='recette' id='recette' value='<%=film.getRecette()%>' required>
            </div>
            <button type='submit'>Enregistrer</button>
        </form>
        <%
            }
            else {
        %>

        <h1>No Film record found.</h1>

        <% } %>
    </body>
</html>