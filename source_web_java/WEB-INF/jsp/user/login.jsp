<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import ="model.*" %>
<%@ page import ="session.*" %>
<html>
    <body>
        <form action='login' method='post'>
            <div>
                <label for='login'>Login :</label>
                <input type='text' name='login' id='login' required>
            </div>
            <div>
                <label for='password'>Mot de passe :</label>
                <input type='password' name='password' id='password' required>
            </div>
            <button type='submit'>Enregistrer</button>
        </form>
        <div><%=request.getAttribute("session")%></div>
    </body>
</html>