package dao;

import model.Acteur;
import model.Film;

import java.sql.*;
import java.util.ArrayList;

/**
 * The type Dao film.
 */
public class DAOFilm {
    /**
     * The Connect.
     */
    public Connection connect;

    /**
     * Instantiates a new Dao film.
     *
     * @throws SQLException           the sql exception
     * @throws ClassNotFoundException the class not found exception
     */
    public DAOFilm() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        this.connect = DriverManager.getConnection("jdbc:mysql://localhost/bdd_exos_java?user=hearthstone&password=1234&useSSL=false"); // localhost
    }

    /**
     * Lister array list.
     *
     * @return the array list
     */
    public ArrayList<Film> Lister() {
        ArrayList<Film> arrayList = new ArrayList<>();

        try {
            DAOActeur daoActeur = new DAOActeur();
            Statement statement = this.connect.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM film");

            while(result.next()) {
                Film film = new Film(
                        result.getInt("id"),
                        result.getString("titre"),
                        Integer.parseInt(result.getString("sortie")),
                        result.getInt("numero"),
                        result.getInt("cout"),
                        result.getInt("recette")
                );

                film.setActeurs(
                        daoActeur.ListerParFilm(film)
                );

                arrayList.add(film);
            }

            daoActeur.fermerLaConnection();
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }

        return arrayList;
    }


    /**
     * Find array list.
     *
     * @param id the id
     * @return the array list
     */
    public Film find(int id) {
        Film film = null;

        try {
            DAOActeur daoActeur = new DAOActeur();
            PreparedStatement statement = this.connect.prepareStatement("SELECT * FROM film WHERE id = ?", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            if(result.first()) {
                film = new Film(
                        result.getInt("id"),
                        result.getString("titre"),
                        Integer.parseInt(result.getString("sortie")),
                        result.getInt("numero"),
                        result.getInt("cout"),
                        result.getInt("recette")
                );

                film.setActeurs(
                        daoActeur.ListerParFilm(film)
                );
            }

            daoActeur.fermerLaConnection();
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }

        return film;
    }

    /**
     * Ajouter.
     *
     * @param film the film
     */
    public void ajouter(Film film) {
        try {
            DAOActeur daoActeur = new DAOActeur();
            PreparedStatement statement = this.connect.prepareStatement("INSERT INTO film(id, titre, sortie, numero, cout, recette) VALUES (?, ?, ?, ?, ?, ?)");
            statement.setInt(1, film.getId());
            statement.setString(2, film.getTitre());
            statement.setString(3, Integer.toString(film.getDateDeSortie()));
            statement.setInt(4, film.getNumeroEpisode());
            statement.setInt(5, film.getCout());
            statement.setInt(6, film.getRecette());

            int affectedRows = statement.executeUpdate();
            System.out.println(affectedRows);

            if (affectedRows != 0) {
                for (Acteur acteur : film.getActeurs()) {
                    daoActeur.ajouter(acteur, film);
                }
            }

            daoActeur.fermerLaConnection();
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Supprimer.
     *
     * @param film the film
     */
    public void supprimer(Film film) {
        try {
            DAOActeur daoActeur = new DAOActeur();

            for(Acteur acteur : film.getActeurs()) {
                daoActeur.supprimer(acteur);
            }

            PreparedStatement statement = this.connect.prepareStatement("DELETE FROM film WHERE id = ?");
            statement.setInt(1, film.getId());

            int affectedRows = statement.executeUpdate();
            System.out.println(affectedRows);
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Fermer la connection.
     *
     * @throws SQLException the sql exception
     */
    public void fermerLaConnection() throws SQLException {
        this.connect.close();
    }

    public boolean update(Film film) {
        try {
            PreparedStatement statement = this.connect.prepareStatement("UPDATE film " +
                    "SET titre = ?, sortie = ?, numero = ?, cout = ?, recette = ? " +
                    "WHERE id = ?");
            statement.setString(1, film.getTitre());
            statement.setString(2, Integer.toString(film.getDateDeSortie()));
            statement.setInt(3, film.getNumeroEpisode());
            statement.setInt(4, film.getCout());
            statement.setInt(5, film.getRecette());
            statement.setInt(6, film.getId());

            int affectedRows = statement.executeUpdate();

            return affectedRows != 0;
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
