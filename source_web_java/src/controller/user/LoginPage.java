package controller.user;

import dao.DAOFilm;
import model.Film;
import session.SessionHandler;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class LoginPage extends HttpServlet {


    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user/login.jsp");
        dispatcher.forward(request, response);
    }


    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        try {
            SessionHandler sessionHandler = new SessionHandler(req);
            req.setAttribute("session", sessionHandler.getAcces());
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }

        processRequest(req, res);
    }


    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        res.setContentType("text/html");
        PrintWriter pw=res.getWriter();

        // params
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        pw.println("<html><body>");


        try {
            SessionHandler sessionHandler = new SessionHandler(req);
             boolean success = sessionHandler.login(login, password);

            pw.println(sessionHandler);
            pw.println(success ? "<br>login successful" : "<br>login failed");
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            pw.println("<h1>An error happened when the program created a new session<h1>");
        }


        pw.println("</body></html>");
        pw.close();//closing the stream
    }
}
