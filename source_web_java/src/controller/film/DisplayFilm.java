package controller.film;

import dao.DAOFilm;
import model.Film;

import javax.servlet.http.*;
import javax.servlet.*;

import java.io.*;
import java.sql.SQLException;

/**
 * The type Display film.
 */
public class DisplayFilm extends HttpServlet {
    public void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException,IOException {

        res.setContentType("text/html");//setting the content type
        PrintWriter pw=res.getWriter();//get the stream to write the data

        pw.println("<html><body><a href='addFilm'>Create a new Film here</a><br>");


        try {
            DAOFilm daoFilm = new DAOFilm();

            pw.println("<ul>");
            for (Film film : daoFilm.Lister()) {
                pw.println("\t<li>");
                pw.println("\t\t"
                        + film.getId() + ", "
                        + film.getTitre() + ", "
                        + film.getDateDeSortie() + ", "
                        + film.getCout() + ", "
                        + film.getRecette() + ", "
                        + film.getActeurs() + " - "
                        + "<a href='updateFilm?id="+film.getId()+"'>Update this film</a>"
                );
                pw.println("\t</li>");
            }
            pw.println("</ul>");

            daoFilm.fermerLaConnection();
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
            pw.println("<center>Could not load the Film table</center>");
        }

        pw.println("</body></html>");
        pw.close();//closing the stream
    }
}