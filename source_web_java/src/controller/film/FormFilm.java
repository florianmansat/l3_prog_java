package controller.film;

import dao.DAOFilm;
import model.Film;
import session.SessionHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

/**
 * The type Form film.
 */
public class FormFilm extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        // snippet to initialise the session and redirect if it is not valid
        try {
            SessionHandler sessionHandler = new SessionHandler(req);

            if (!sessionHandler.isAvalidSession()) {
                res.sendRedirect(req.getContextPath() + "/login");
            }
            else {

                res.setContentType("text/html");//setting the content type
                PrintWriter pw=res.getWriter();//get the stream to write the data

                pw.println("<html><body>");

                pw.println("<form action='addFilm' method='post'>" +
                        "<div>\n" +
                        "  <label for='idFilm'>Id :</label>\n" +
                        "  <input type='number' name='idFilm' id='idFilm' required>\n" +
                        "</div>\n" +
                        "<div>\n" +
                        "  <label for='name'>Titre :</label>\n" +
                        "  <input type='text' name='titre' id='titre' required>\n" +
                        "</div>\n" +
                        "<div>\n" +
                        "  <label for='name'>Date de sortie :</label>\n" +
                        "  <input type='number' name='dateDeSortie' id='dateDeSortie' required>\n" +
                        "</div>\n" +
                        "<div>\n" +
                        "  <label for='name'>Numéro de l'épisode :</label>\n" +
                        "  <input type='number' name='numero' id='numero' required>\n" +
                        "</div>\n" +
                        "<div>\n" +
                        "  <label for='name'>Coût :</label>\n" +
                        "  <input type='number' name='cout' id='cout' required>\n" +
                        "</div>\n" +
                        "<div>\n" +
                        "  <label for='name'>Recette :</label>\n" +
                        "  <input type='number' name='recette' id='recette' required>\n" +
                        "</div>\n" +
                        "<button type='submit'>Enregistrer</button>" +
                        "</form>");

                pw.println("</body></html>");
                pw.close();//closing the stream

            }
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }

        // -----------------------------------------------------------------
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        // snippet to initialise the session and redirect if it is not valid
        try {
            SessionHandler sessionHandler = new SessionHandler(req);

            if (!sessionHandler.isAvalidSession()) {
                res.sendRedirect(req.getContextPath() + "/login");
            }
            else {

                res.setContentType("text/html");
                PrintWriter pw=res.getWriter();

                // params
                int idFilm = Integer.parseInt(req.getParameter("idFilm"));
                String titre = req.getParameter("titre");
                int dateDeSortie = Integer.parseInt(req.getParameter("dateDeSortie"));
                int numero = Integer.parseInt(req.getParameter("numero"));
                int cout = Integer.parseInt(req.getParameter("cout"));
                int recette = Integer.parseInt(req.getParameter("recette"));


                try {
                    DAOFilm daoFilm = new DAOFilm();

                    daoFilm.ajouter(
                            new Film(
                                    idFilm,
                                    titre,
                                    dateDeSortie,
                                    numero,
                                    cout,
                                    recette
                            )
                    );

                    pw.println("<center>Sucessfully created a film.<br>You can go back to see the new film <a href='film'>here</a></center>");
                    daoFilm.fermerLaConnection();
                }
                catch (SQLException | ClassNotFoundException throwables) {
                    throwables.printStackTrace();
                    pw.println("<center>Could not create a Film</center>");
                }


                pw.println("</body></html>");
                pw.close();//closing the stream

            }
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        // -----------------------------------------------------------------
    }
}
