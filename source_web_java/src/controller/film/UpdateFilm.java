package controller.film;

import dao.DAOFilm;
import model.Film;
import session.SessionHandler;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class UpdateFilm extends HttpServlet {

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/film/modify.jsp");
        dispatcher.forward(request, response);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        // snippet to initialise the session and redirect if it is not valid
        try {
            SessionHandler sessionHandler = new SessionHandler(req);

            // if not logged in redirect to the login page
            if (!sessionHandler.isAvalidSession()) {
                res.sendRedirect(req.getContextPath() + "/login");
            }
            // else proceed
            else {

                try {
                    int id = Integer.parseInt(req.getParameter("id"));
                    DAOFilm daoFilm = new DAOFilm();

                    Film film = daoFilm.find(id);
                    req.setAttribute("film", film);
                }
                catch (NumberFormatException | SQLException | ClassNotFoundException throwables) {
                    throwables.printStackTrace();
                }

                processRequest(req, res);

            }
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        // -----------------------------------------------------------------
    }


    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        // snippet to initialise the session and redirect if it is not valid
        try {
            SessionHandler sessionHandler = new SessionHandler(req);

            if (!sessionHandler.isAvalidSession()) {
                res.sendRedirect(req.getContextPath() + "/login");
            }
            else {

                res.setContentType("text/html");
                PrintWriter pw=res.getWriter();

                // params
                int idFilm = Integer.parseInt(req.getParameter("idFilm"));
                String titre = req.getParameter("titre");
                int dateDeSortie = Integer.parseInt(req.getParameter("dateDeSortie"));
                int numero = Integer.parseInt(req.getParameter("numero"));
                int cout = Integer.parseInt(req.getParameter("cout"));
                int recette = Integer.parseInt(req.getParameter("recette"));


                try {
                    DAOFilm daoFilm = new DAOFilm();

                    daoFilm.update(
                            new Film(
                                    idFilm,
                                    titre,
                                    dateDeSortie,
                                    numero,
                                    cout,
                                    recette
                            )
                    );

                    pw.println("<center>Sucessfully updated a film.<br>You can go back to see all the film <a href='film'>here</a></center>");
                    daoFilm.fermerLaConnection();
                }
                catch (SQLException | ClassNotFoundException throwables) {
                    throwables.printStackTrace();
                    pw.println("<center>Could not update a Film</center>");
                }


                pw.println("</body></html>");
                pw.close();//closing the stream

            }
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        // -----------------------------------------------------------------
    }
}
