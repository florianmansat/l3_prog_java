package controller;

import session.SessionHandler;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class IndexPage extends HttpServlet {

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
        dispatcher.forward(request, response);
    }


    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        // snippet to initialise the session
        try {
            SessionHandler sessionHandler = new SessionHandler(req);
            req.setAttribute("session", sessionHandler.getAcces());
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }

        processRequest(req, res);
    }
}
