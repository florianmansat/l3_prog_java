package session;

import dao.DAOUser;
import model.Acces;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;

/**
 * The type Session handler.
 */
public class SessionHandler {
    private HttpSession httpSession;
    private DAOUser daoUser;
    private Acces acces;

    /**
     * Instantiates a new Session handler.
     *
     * @param request the request
     * @throws SQLException           the sql exception
     * @throws ClassNotFoundException the class not found exception
     */
    public SessionHandler(HttpServletRequest request) throws SQLException, ClassNotFoundException {
        this.daoUser = new DAOUser();
        httpSession = request.getSession();

        if (httpSession.getAttribute("login") != null && httpSession.getAttribute("password") != null) {
            if (!this.login((String) httpSession.getAttribute("login"),
                    (String) httpSession.getAttribute("password"))) {
                httpSession.invalidate();
            }
        }
    }

    /**
     * Is avalid session boolean.
     *
     * @return the boolean
     */
    public boolean isAvalidSession() {
        if (acces == null) {
            return false;
        }

        return this.daoUser.login(
                acces.getLogin(),
                acces.getPassword()
        ) != null;
    }

    /**
     * Login boolean.
     *
     * @param login    the login
     * @param password the password
     * @return the boolean
     */
    public boolean login(String login, String password) {
        acces = this.daoUser.login(
                login,
                password
        );

        if (acces != null) {
            httpSession.setAttribute("login", acces.getLogin());
            httpSession.setAttribute("password", acces.getPassword());
        }

        return acces != null;
    }

    /**
     * Register boolean.
     *
     * @param acces the acces
     * @return the boolean
     */
    public boolean register(Acces acces) {
        boolean registered = daoUser.register(acces);

        if (registered) {
            this.acces = acces;

            httpSession.setAttribute("login", acces.getLogin());
            httpSession.setAttribute("password", acces.getPassword());
        }

        return registered;
    }

    /**
     * Gets acces.
     *
     * @return the acces
     */
    public Acces getAcces() {
        return acces;
    }

    /**
     * Sets acces.
     *
     * @param acces the acces
     */
    public void setAcces(Acces acces) {
        this.acces = acces;
    }

    @Override
    public String toString() {
        return "SessionHandler{" +
                "acces=" + acces +
                '}';
    }
}
