package model;

/**
 * The type Acces.
 */
public class Acces {
    private int id;
    private String prenom;
    private String login;
    private String password;
    private String statut;
    private int age;

    /**
     * Instantiates a new Acces.
     *
     * @param id       the id
     * @param prenom   the prenom
     * @param login    the login
     * @param password the password
     * @param statut   the statut
     * @param age      the age
     */
    public Acces(int id, String prenom, String login, String password, String statut, int age) {
        this.id = id;
        this.prenom = prenom;
        this.login = login;
        this.password = password;
        this.statut = statut;
        this.age = age;
    }

    /**
     * Instantiates a new Acces.
     */
    public Acces() {
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Sets prenom.
     *
     * @param prenom the prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets statut.
     *
     * @return the statut
     */
    public String getStatut() {
        return statut;
    }

    /**
     * Sets statut.
     *
     * @param statut the statut
     */
    public void setStatut(String statut) {
        this.statut = statut;
    }

    /**
     * Gets age.
     *
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * Sets age.
     *
     * @param age the age
     */
    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Acces{" +
                "id=" + id +
                ", prenom='" + prenom + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", statut='" + statut + '\'' +
                ", age=" + age +
                '}';
    }
}
