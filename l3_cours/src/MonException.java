/**
 * The type Mon exception.
 */
public class MonException extends Exception {
    /**
     * Instantiates a new Mon exception.
     */
    public MonException() {
    }

    /**
     * Instantiates a new Mon exception.
     *
     * @param message the message
     */
    public MonException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Mon exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public MonException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Mon exception.
     *
     * @param cause the cause
     */
    public MonException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Mon exception.
     *
     * @param message            the message
     * @param cause              the cause
     * @param enableSuppression  the enable suppression
     * @param writableStackTrace the writable stack trace
     */
    public MonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
