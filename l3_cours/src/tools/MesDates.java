package tools;

/**
 * The type Mes dates.
 */
public class MesDates {

    /**
     * Instantiates a new Mes dates.
     */
    public MesDates() {}

    /**
     * Date fr to us string.
     *
     * @param date the date
     * @return the string
     */
    public static String dateFRtoUS(String date) {
        String[] array = date.split("/");

        return array[1]+"-"+array[0]+"-"+array[2];
    }

    /**
     * Date us to fr string.
     *
     * @param date the date
     * @return the string
     */
    public static String dateUStoFR(String date) {
        String[] array = date.split("-");

        return array[1]+"/"+array[0]+"/"+array[2];
    }
}
