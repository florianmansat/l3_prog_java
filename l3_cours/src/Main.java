import tools.MesDates;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.Scanner;

/**
 * The Main class.
 */
public class Main {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Visite visite1 = new Visite();
        Visite visite2 = new Visite("Moi", new Date("20/04/2021"), 15);

        System.out.println(visite1.toString());
        System.out.println(visite1.getEtudiant());
        System.out.println(visite2.toString());
        System.out.println(visite2.getEtudiant());


        Integer integer = 5;
        int anotherInt = (int) integer;
        String str = integer.toString();
        int differentInt = Integer.parseInt(str);


        String date = "20/04/2021";
        System.out.println("Date FR : "+date);
        String dateUS = MesDates.dateFRtoUS(date);
        System.out.println("Date FR=>US : "+dateUS);
        System.out.println("Date US=>FR : "+MesDates.dateUStoFR(dateUS));


        Planning plan = new Planning();
        System.out.println(plan);
        plan.AjoutVisite(visite2);
        System.out.println(plan);
        plan.SupprVisite("Moi");
        System.out.println(plan);

        Calcul calcul = new Calcul(5, 5);
        System.out.println(calcul.caculAddition());
        System.out.println(calcul.caculMultiplication());
        System.out.println(calcul.caculDivision());
        calcul = new Calcul(5, 0);
        System.out.println(calcul.caculDivision());


        System.out.println("Contenu du fichier fichier.txt : ");
        readFile("src/fichier.txt");

//        int tab[ ] = int [] new;
        int tab[ ] = new int[4];

        int i = 2;
        System.out.print(++i * 6);
    }

    /**
     * Read file.
     *
     * @param path the path
     */
    public static void readFile(String path) {
        try {
            File file = new File(path);
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                String data = scanner.nextLine();
                System.out.println("\t"+data);
            }
            scanner.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("Le fichier n'a pas été trouvé.");
            e.printStackTrace();
        }
    }
}
