import java.util.Date;

/**
 * The type Visite.
 */
public class Visite {
    private String etudiant;
    private Date date;
    private int note;

    /**
     * Instantiates a new Visite.
     *
     * @param etudiant the etudiant
     * @param date     the date
     * @param note     the note
     */
    public Visite(String etudiant, Date date, int note) {
        this.etudiant = etudiant;
        this.date = date;
        this.note = note;
    }

    /**
     * Instantiates a new Visite.
     */
    public Visite() {}

    /**
     * Gets etudiant.
     *
     * @return the etudiant
     */
    public String getEtudiant() {
        return etudiant;
    }

    /**
     * Sets etudiant.
     *
     * @param etudiant the etudiant
     */
    public void setEtudiant(String etudiant) {
        this.etudiant = etudiant;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets note.
     *
     * @return the note
     */
    public int getNote() {
        return note;
    }

    /**
     * Sets note.
     *
     * @param note the note
     */
    public void setNote(int note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Visite{" +
                "etudiant='" + etudiant + '\'' +
                ", date='" + date + '\'' +
                ", note=" + note +
                '}';
    }
}
