import java.util.ArrayList;

/**
 * The type Planning.
 */
public class Planning {

    /**
     * The Visites.
     */
    ArrayList<Visite> visites;

    /**
     * Instantiates a new Planning.
     *
     * @param visites the visites
     */
    public Planning(ArrayList<Visite> visites) {
        this.visites = visites;
    }

    /**
     * Instantiates a new Planning.
     */
    public Planning() {
        visites = new ArrayList<Visite>();
    }

    /**
     * Gets visites.
     *
     * @return the visites
     */
    public ArrayList<Visite> getVisites() {
        return visites;
    }

    /**
     * Sets visites.
     *
     * @param visites the visites
     */
    public void setVisites(ArrayList<Visite> visites) {
        this.visites = visites;
    }

    /**
     * Ajout visite.
     *
     * @param visite the visite
     */
    public void AjoutVisite(Visite visite) {
        visites.add(visite);
    }

    /**
     * Suppr visite.
     *
     * @param etudiant the etudiant
     */
    public void SupprVisite(String etudiant) {
        for (int i = 0; i < visites.size(); i++) {
            if (etudiant.equals(visites.get(i).getEtudiant())) {
                visites.remove(visites.get(i));
            }
        }
    }

    @Override
    public String toString() {
        return "Planning{" +
                "visites=" + visites +
                '}';
    }
}
