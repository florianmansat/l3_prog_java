import javax.swing.*;

/**
 * The type Calcul.
 */
public class Calcul {
    /**
     * The Entier 1.
     */
    int entier1;
    /**
     * The Entier 2.
     */
    int entier2;

    /**
     * Instantiates a new Calcul.
     *
     * @param entier1 the entier 1
     * @param entier2 the entier 2
     */
    public Calcul(int entier1, int entier2) {
        this.entier1 = entier1;
        this.entier2 = entier2;
    }

    /**
     * Instantiates a new Calcul.
     */
    public Calcul() {}

    /**
     * Gets entier 1.
     *
     * @return the entier 1
     */
    public int getEntier1() {
        return entier1;
    }

    /**
     * Cacul addition int.
     *
     * @return the int
     */
    public int caculAddition() {
        return entier1 + entier2;
    }

    /**
     * Cacul multiplication double.
     *
     * @return the double
     */
    public double caculMultiplication() {
        return entier1 * entier2;
    }

    /**
     * Cacul division double.
     *
     * @return the double
     */
    public double caculDivision() {
        try {
            return entier1 / entier2;
        }
        catch (ArithmeticException ex) {
            System.out.println("Division par zero impossible");
            return 0;
        }
    }

    /**
     * Sets entier 1.
     *
     * @param entier1 the entier 1
     */
    public void setEntier1(int entier1) {
        this.entier1 = entier1;
    }

    /**
     * Gets entier 2.
     *
     * @return the entier 2
     */
    public int getEntier2() {
        return entier2;
    }

    /**
     * Sets entier 2.
     *
     * @param entier2 the entier 2
     */
    public void setEntier2(int entier2) {
        this.entier2 = entier2;
    }

    @Override
    public String toString() {
        return "Calcul{" +
                "entier1=" + entier1 +
                ", entier2=" + entier2 +
                '}';
    }
}
