import java.util.HashMap;

/**
 * The type Acteur.
 */
public class Acteur {
    private String nom;
    private String prenom;
    private HashMap<String, Personnage> listePersonnage;

    /**
     * Instantiates a new Acteur.
     *
     * @param nom    the nom
     * @param prenom the prenom
     */
    public Acteur(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
        this.listePersonnage = new HashMap<>();
    }

    /**
     * Instantiates a new Acteur.
     */
    public Acteur() {
    }

    /**
     * Gets nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets nom.
     *
     * @param nom the nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Gets prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Sets prenom.
     *
     * @param prenom the prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Gets liste personnage.
     *
     * @return the liste personnage
     */
    public HashMap<String, Personnage> getListePersonnage() {
        return listePersonnage;
    }

    /**
     * Sets liste personnage.
     *
     * @param listePersonnage the liste personnage
     */
    public void setListePersonnage(HashMap<String, Personnage> listePersonnage) {
        this.listePersonnage = listePersonnage;
    }

    /**
     * Ajouter personnage.
     *
     * @param nomFilm    the nom film
     * @param personnage the personnage
     */
    public void ajouterPersonnage(String nomFilm, Personnage personnage) {
        this.listePersonnage.put(nomFilm, personnage);
    }

    /**
     * Nb personnages int.
     *
     * @return the int
     */
    public int nbPersonnages() {
        return this.listePersonnage.size();
    }

    @Override
    public String toString() {
        return "Acteur{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", listePersonnage=" + listePersonnage +
                '}';
    }
}
