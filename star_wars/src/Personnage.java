/**
 * The type Personnage.
 */
public class Personnage {
    private String nom;
    private String prenom;

    /**
     * Instantiates a new Personnage.
     *
     * @param nom    the nom
     * @param prenom the prenom
     */
    public Personnage(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    /**
     * Instantiates a new Personnage.
     */
    public Personnage() {}

    /**
     * Gets nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets nom.
     *
     * @param nom the nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Gets prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Sets prenom.
     *
     * @param prenom the prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public String toString() {
        return "Personnage{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                '}';
    }
}
