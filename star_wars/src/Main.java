import java.util.*;

/**
 * The type Main.
 */
public class Main {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Scanner scan= new Scanner(System.in);

        Film film1 = new Film("Star Wars, épisode IV : Un nouvel espoir", 1977, 4, 1000000, 20000000);
        Film film2 = new Film("Star Wars, épisode V : L'Empire contre-attaque", 1980, 5, 3400000, 45000000);
        Film film3 = new Film();


        System.out.println("Titre : ");
        // film3.setTitre(scan.nextLine());
        film3.setTitre("Test");
        System.out.println("Date de sortie : ");
        // film3.setDateDeSortie(scan.nextInt());
        film3.setDateDeSortie(1985);
        System.out.println("Numero de l'épisode : ");
        // film3.setNumeroEpisode(scan.nextInt());
        film3.setNumeroEpisode(1);
        System.out.println("Cout : ");
        // film3.setCout(scan.nextInt());
        film3.setCout(2);
        System.out.println("Recette : ");
        // film3.setRecette(scan.nextInt());
        film3.setRecette(3);


        System.out.println(film1);
        System.out.println(film2);
        System.out.println(film3);

        // --------------------------------------------------

        Personnage personnage = new Personnage("Solo", "Han");

        System.out.println(personnage);

        // --------------------------------------------------

        ArrayList<Film> arrayFilm = new ArrayList<>();
        arrayFilm.add(film1);
        arrayFilm.add(film2);
        arrayFilm.add(film3);

        System.out.println(arrayFilm);
        afficherListeFilm(arrayFilm);

        // --------------------------------------------------

        Acteur acteur = new Acteur("Ford", "Harrisson");
        acteur.ajouterPersonnage("Star Wars, épisode IV : Un nouvel espoir", personnage);
        acteur.ajouterPersonnage("Indiana Jones", new Personnage("Indiana", "Jones"));

        System.out.println(acteur);

        // --------------------------------------------------

        System.out.println("Nombre de personne : " + acteur.nbPersonnages());

        // --------------------------------------------------

        arrayFilm.get(0).getActeurs().add(acteur);

        System.out.println(arrayFilm.get(0));

        // --------------------------------------------------

        System.out.println(film1.nbActeurs());
        System.out.println(film1.nbPersonnages());
        System.out.println(Arrays.toString(film1.calculBenefice()));
        System.out.println(film1.isBefore(1976));

        // --------------------------------------------------

        HashMap<Integer, Film> dict = new HashMap<>();

        dict.put(film1.getDateDeSortie(), film1);
        dict.put(film2.getDateDeSortie(), film2);
        dict.put(film3.getDateDeSortie(), film3);

        makeBackUp(dict);
    }

    /**
     * Afficher liste film.
     *
     * @param liste the liste
     */
    public static void afficherListeFilm(ArrayList<Film> liste) {
        for (Film film : liste) {
            System.out.println(film);
        }
    }

    /**
     * Make back up.
     *
     * @param dictionary the dictionary
     */
    public static void makeBackUp(HashMap<Integer, Film> dictionary) {
        for (Map.Entry<Integer, Film> e  :  dictionary.entrySet()) {
            System.out.println(
                    e.getKey()+" - "+e.getValue().getTitre() + " - " + e.getValue().calculBenefice()[0]
            );
        }
    }
}
