/**
 * The type Mechant.
 */
public class Mechant extends Personnage {
    private boolean coteObscur;

    /**
     * Instantiates a new Mechant.
     *
     * @param nom        the nom
     * @param prenom     the prenom
     * @param acteur     the acteur
     * @param coteObscur the cote obscur
     */
    public Mechant(String nom, String prenom, Acteur acteur, boolean coteObscur) {
        super(nom, prenom);
        this.coteObscur = coteObscur;
    }

    /**
     * Instantiates a new Mechant.
     *
     * @param coteObscur the cote obscur
     */
    public Mechant(boolean coteObscur) {
        this.coteObscur = coteObscur;
    }

    /**
     * Instantiates a new Mechant.
     */
    public Mechant() {}

    /**
     * Is cote obscur boolean.
     *
     * @return the boolean
     */
    public boolean isCoteObscur() {
        return coteObscur;
    }

    /**
     * Sets cote obscur.
     *
     * @param coteObscur the cote obscur
     */
    public void setCoteObscur(boolean coteObscur) {
        this.coteObscur = coteObscur;
    }

    @Override
    public String toString() {
        return "Mechant{" +
                "coteObscur=" + coteObscur +
                '}';
    }
}
