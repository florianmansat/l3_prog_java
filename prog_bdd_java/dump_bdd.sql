-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour bdd_exos_java
CREATE DATABASE IF NOT EXISTS `bdd_exos_java` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bdd_exos_java`;

-- Listage de la structure de la table bdd_exos_java. acces
CREATE TABLE IF NOT EXISTS `acces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(20) NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `statut` varchar(20) NOT NULL,
  `age` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Listage des données de la table bdd_exos_java.acces : ~4 rows (environ)
/*!40000 ALTER TABLE `acces` DISABLE KEYS */;
INSERT INTO `acces` (`id`, `prenom`, `login`, `password`, `statut`, `age`) VALUES
	(1, 'Tom', 'tomahawk', 'indien', 'Etudiant', 22),
	(2, 'Pierre', 'Pierrot', 'delalune', 'Prof', 44),
	(3, 'Michel', 'lamere', 'sonchat', 'Admin', 69),
	(4, 'Robin', 'Locksley', 'desbois', 'Etudiant', 23),
	(5, 'Tom', 'tomahawk', 'indien', 'Etudiant', 22);
/*!40000 ALTER TABLE `acces` ENABLE KEYS */;

-- Listage de la structure de la table bdd_exos_java. acteur
CREATE TABLE IF NOT EXISTS `acteur` (
  `id` int(11) NOT NULL,
  `filmId` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_acteur_film` (`filmId`),
  CONSTRAINT `FK_acteur_film` FOREIGN KEY (`filmId`) REFERENCES `film` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table bdd_exos_java.acteur : ~0 rows (environ)
/*!40000 ALTER TABLE `acteur` DISABLE KEYS */;
INSERT INTO `acteur` (`id`, `filmId`, `nom`, `prenom`) VALUES
	(0, 0, 't', 'a');
/*!40000 ALTER TABLE `acteur` ENABLE KEYS */;

-- Listage de la structure de la table bdd_exos_java. film
CREATE TABLE IF NOT EXISTS `film` (
  `id` int(11) NOT NULL,
  `titre` varchar(50) NOT NULL,
  `sortie` varchar(50) NOT NULL,
  `numero` int(11) NOT NULL,
  `cout` int(11) NOT NULL,
  `recette` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table bdd_exos_java.film : ~2 rows (environ)
/*!40000 ALTER TABLE `film` DISABLE KEYS */;
INSERT INTO `film` (`id`, `titre`, `sortie`, `numero`, `cout`, `recette`) VALUES
	(0, 'aled', '1998', 654, 30000, 1000000),
	(1, 'test', '1998', 654, 30000, 1000000);
/*!40000 ALTER TABLE `film` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
