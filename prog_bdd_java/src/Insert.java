import java.sql.*;

public class Insert {
    public static void main(String[] args) {
        try {
            String dbName= "bdd_exos_java";
            String login= "hearthstone";
            String motdepasse= "1234";
            String strUrl = "jdbc:mysql://localhost/" +  dbName;

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(strUrl, login, motdepasse);
            PreparedStatement statement = conn.prepareStatement("INSERT INTO film(titre, sortie, numero, cout, recette) VALUES ('Test','test', 3, 400, 20000)");

            int affectedRows = statement.executeUpdate();
            System.out.println(affectedRows);

            conn.close();
        }
        catch(ClassNotFoundException e) {
            System.err.println("Driver non charg� !");  e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("Autre erreur !");  e.printStackTrace();
        }
    }
}
