import dao.DAOFilm;
import model.Acteur;
import model.Film;

import java.sql.*;

public class Main {


    public static void main(String[] args) {
        try {
            DAOFilm dao = new DAOFilm();

            Film film = new Film(2, "Test", 1933, 23, 3000, 400000);
            film.getActeurs().add(new Acteur(3, "aled", "aled"));
            System.out.println(film);


            System.out.println("Avant l'insertion :");
            System.out.println(dao.Lister());
            dao.ajouter(film);

            System.out.println("Apres l'insertion :");
            System.out.println(dao.Lister());

            System.out.println("Apres la suppression :");
            dao.supprimer(film);
            System.out.println(dao.Lister());
        }
        catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    }
}
