package dao;

import model.Acces;
import model.Film;

import java.sql.*;
import java.util.ArrayList;

/**
 * The type Dao acces.
 */
public class DAOAcces {
    /**
     * The Connect.
     */
    public Connection connect;

    /**
     * Instantiates a new Dao acces.
     *
     * @throws SQLException           the sql exception
     * @throws ClassNotFoundException the class not found exception
     */
    public DAOAcces() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        this.connect = DriverManager.getConnection("jdbc:mysql://localhost/bdd_exos_java?user=hearthstone&password=1234&useSSL=false"); // localhost
    }

    /**
     * Lister array list.
     *
     * @return the array list
     */
    public ArrayList<Acces> Lister() {
        ArrayList<Acces> arrayList = new ArrayList<>();

        try {
            Statement statement = this.connect.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM acces");

            while(result.next()) {
                arrayList.add(
                        new Acces(
                                result.getInt("id"),
                                result.getString("prenom"),
                                result.getString("login"),
                                result.getString("password"),
                                result.getString("statut"),
                                result.getInt("age")
                        )
                );
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return arrayList;
    }

    /**
     * Ajouter.
     *
     * @param acces the acces
     */
    public void ajouter(Acces acces) {
        try {
            PreparedStatement statement = this.connect.prepareStatement("INSERT INTO acces VALUES (?, ?, ?, ?, ?, ?)");
            statement.setInt(1, acces.getId());
            statement.setString(2, acces.getPrenom());
            statement.setString(3, acces.getLogin());
            statement.setString(4, acces.getPrenom());
            statement.setString(5, acces.getStatut());
            statement.setInt(6, acces.getAge());

            int affectedRows = statement.executeUpdate();
            System.out.println(affectedRows);
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Supprimer.
     *
     * @param acces the acces
     */
    public void supprimer(Acces acces) {
        try {
            PreparedStatement statement = this.connect.prepareStatement("DELETE FROM acces WHERE id = ?");
            statement.setInt(1, acces.getId());

            int affectedRows = statement.executeUpdate();
            System.out.println(affectedRows);
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Fermer la connection.
     *
     * @throws SQLException the sql exception
     */
    public void fermerLaConnection() throws SQLException {
        this.connect.close();
    }
}
