import java.sql.*;

public class Select {
    public static void main(String[] args) {
        try {
            String dbName= "bdd_exos_java";
            String login= "hearthstone";
            String motdepasse= "1234";
            String strUrl = "jdbc:mysql://localhost/" +  dbName;

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(strUrl, login, motdepasse);
            Statement statement = conn.createStatement();

            ResultSet result = statement.executeQuery("SELECT * FROM film");


            while(result.next()) {
                System.out.println(
                        result.getInt("id") + ", "
                        + result.getString("titre") + ", "
                        + result.getString("sortie") + ", "
                        + result.getInt("numero") + ", "
                        + result.getInt("cout") + ", "
                        + result.getInt("recette")
                );
            }

            conn.close();
        }
        catch(ClassNotFoundException e) {
            System.err.println("Driver non charg� !");  e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("Autre erreur !");  e.printStackTrace();
        }
    }
}
