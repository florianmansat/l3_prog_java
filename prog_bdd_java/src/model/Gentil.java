package model;

/**
 * The type Gentil.
 */
public class Gentil extends Personnage {
    private boolean force;

    /**
     * Instantiates a new Gentil.
     *
     * @param nom    the nom
     * @param prenom the prenom
     * @param acteur the acteur
     * @param force  the force
     */
    public Gentil(String nom, String prenom, Acteur acteur, boolean force) {
        super(nom, prenom);
        this.force = force;
    }

    /**
     * Instantiates a new Gentil.
     *
     * @param force the force
     */
    public Gentil(boolean force) {
        this.force = force;
    }

    /**
     * Instantiates a new Gentil.
     */
    public Gentil() {}

    /**
     * Is force boolean.
     *
     * @return the boolean
     */
    public boolean isForce() {
        return force;
    }

    /**
     * Sets force.
     *
     * @param force the force
     */
    public void setForce(boolean force) {
        this.force = force;
    }

    @Override
    public String toString() {
        return "Gentil{" +
                "force=" + force +
                '}';
    }
}
