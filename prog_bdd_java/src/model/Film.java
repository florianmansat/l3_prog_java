package model;

import java.lang.reflect.Array;
import java.util.*;

/**
 * The type Film.
 */
public class Film {
    private int id;
    private String titre;
    private int dateDeSortie;
    private int numeroEpisode;
    private int cout;
    private int recette;
    private ArrayList<Acteur> acteurs;

    /**
     * Instantiates a new Film.
     *
     * @param id            the id
     * @param titre         the titre
     * @param dateDeSortie  the date de sortie
     * @param numeroEpisode the numero episode
     * @param cout          the cout
     * @param recette       the recette
     * @param acteurs       the acteurs
     */
    public Film(int id, String titre, int dateDeSortie, int numeroEpisode, int cout, int recette, ArrayList<Acteur> acteurs) {
        this.id = id;
        this.titre = titre;
        this.dateDeSortie = dateDeSortie;
        this.numeroEpisode = numeroEpisode;
        this.cout = cout;
        this.recette = recette;
        this.acteurs = acteurs;
    }

    /**
     * Instantiates a new Film.
     *
     * @param id      the id
     * @param titre   the titre
     * @param sortie  the sortie
     * @param numero  the numero
     * @param cout    the cout
     * @param recette the recette
     */
    public Film(int id, String titre, int sortie, int numero, int cout, int recette) {
        this.id = id;
        this.titre = titre;
        this.dateDeSortie = sortie;
        this.numeroEpisode = numero;
        this.cout = cout;
        this.recette = recette;
        this.acteurs = new ArrayList<>();
    }

    /**
     * Instantiates a new Film.
     */
    public Film() {
        this.acteurs = new ArrayList<>();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets titre.
     *
     * @return the titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * Sets titre.
     *
     * @param titre the titre
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * Gets date de sortie.
     *
     * @return the date de sortie
     */
    public int getDateDeSortie() {
        return dateDeSortie;
    }

    /**
     * Sets date de sortie.
     *
     * @param dateDeSortie the date de sortie
     */
    public void setDateDeSortie(int dateDeSortie) {
        this.dateDeSortie = dateDeSortie;
    }

    /**
     * Gets numero episode.
     *
     * @return the numero episode
     */
    public int getNumeroEpisode() {
        return numeroEpisode;
    }

    /**
     * Sets numero episode.
     *
     * @param numeroEpisode the numero episode
     */
    public void setNumeroEpisode(int numeroEpisode) {
        this.numeroEpisode = numeroEpisode;
    }

    /**
     * Gets cout.
     *
     * @return the cout
     */
    public int getCout() {
        return cout;
    }

    /**
     * Sets cout.
     *
     * @param cout the cout
     */
    public void setCout(int cout) {
        this.cout = cout;
    }

    /**
     * Gets recette.
     *
     * @return the recette
     */
    public int getRecette() {
        return recette;
    }

    /**
     * Sets recette.
     *
     * @param recette the recette
     */
    public void setRecette(int recette) {
        this.recette = recette;
    }

    /**
     * Gets acteurs.
     *
     * @return the acteurs
     */
    public ArrayList<Acteur> getActeurs() {
        return acteurs;
    }

    /**
     * Sets acteurs.
     *
     * @param acteurs the acteurs
     */
    public void setActeurs(ArrayList<Acteur> acteurs) {
        this.acteurs = acteurs;
    }

    /**
     * Nb acteurs int.
     *
     * @return the int
     */
    public int nbActeurs() {
        return this.acteurs.size();
    }

    /**
     * Nb personnages int.
     *
     * @return the int
     */
    public int nbPersonnages() {
        int nbPersonnage = 0;

        for (Acteur acteur : acteurs) {
            if (acteur.getListePersonnage().get(this.getTitre()) != null) {
                nbPersonnage += 1;
            }
        }

        return nbPersonnage;
    }

    /**
     * Calcul benefice object [ ].
     *
     * @return the object [ ]
     */
    public Object[] calculBenefice() {
        Object[] arr = new Object[2];

        arr[0] = this.getRecette() - this.getCout();
        arr[1] = (this.getRecette() - this.getCout()) > 0;

        return arr;
    }

    /**
     * Is before boolean.
     *
     * @param annee the annee
     * @return the boolean
     */
    public boolean isBefore(int annee) {
        return annee > this.getDateDeSortie();
    }


    /**
     * Tri.
     */
    public void tri() {
        this.getActeurs().sort(
                Comparator.comparing(Acteur::getNom).thenComparing(Acteur::getPrenom)
        );
    }


    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", dateDeSortie=" + dateDeSortie +
                ", numeroEpisode=" + numeroEpisode +
                ", cout=" + cout +
                ", recette=" + recette +
                ", acteurs=" + acteurs +
                '}';
    }
}
