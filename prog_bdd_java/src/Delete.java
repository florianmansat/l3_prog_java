import java.sql.*;
import java.util.Scanner;

public class Delete {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            String dbName= "bdd_exos_java";
            String login= "hearthstone";
            String motdepasse= "1234";
            String strUrl = "jdbc:mysql://localhost/" +  dbName;

            System.out.println("Id: ");
            int id = scanner.nextInt();

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(strUrl, login, motdepasse);
            PreparedStatement st = conn.prepareStatement("DELETE FROM film WHERE id = ?");
            st.setInt(1, id);

            int affectedRows = st.executeUpdate();
            System.out.println(affectedRows);

            conn.close();
        }
        catch(ClassNotFoundException e) {
            System.err.println("Driver non charg� !");  e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("Autre erreur !");  e.printStackTrace();
        }
    }
}
