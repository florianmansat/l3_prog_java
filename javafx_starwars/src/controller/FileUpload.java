package controller;

import java.awt.Desktop;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import sample.Main;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileUpload implements Initializable {
    public Button buttonFile;
    public ImageView imageView;
    private Desktop desktop = Desktop.getDesktop();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("View Pictures");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home") + "\\Pictures")
        );
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );

        buttonFile.snapPositionX(20);
        buttonFile.snapPositionY(20);
        buttonFile.setOnAction(
                e -> {
                    File file = fileChooser.showOpenDialog(Main.primaryStage);
                    if (file != null) {
                        // openFile(file);
                        imageView.setImage(new Image(file.toURI().toString()));
                    }
                }
        );

        imageView.setFitHeight(500);
        imageView.setFitWidth(500);
        imageView.setPreserveRatio(true);
    }


    private void openFile(File file) {
        try {
            desktop.open(file);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
