package controller;

import dao.DAOFilm;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import model.Film;
import sample.Main;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class FilmList implements Initializable {
    @FXML
    public ListView<Film> listView;
    public Text text;
    public TextField titre;
    public TextField date;
    public TextField numero;
    public TextField cout;
    public TextField recette;
    public Film selectedFilm;
    public GridPane formModify;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            formModify.setVisible(false);
            DAOFilm daoFilm = new DAOFilm();
            ArrayList<Film> arrayList = daoFilm.Lister();


            listView.setCellFactory(param -> new ListCell<Film>() {
                @Override
                protected void updateItem(Film item, boolean empty) {
                    super.updateItem(item, empty);

                    if (empty || item == null || item.getTitre() == null) {
                        setText(null);
                    } else {
                        setText(item.getTitre());
                    }
                }
            });

            listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Film>() {
                @Override
                public void changed(ObservableValue<? extends Film> observableValue, Film oldValue, Film newValue) {
                    formModify.setVisible(true);

                    System.out.println("ListView selection changed from oldValue = " + oldValue + " to newValue = " + newValue);

                    selectedFilm = newValue;

                    titre.setText(newValue.getTitre());
                    date.setText(String.valueOf(newValue.getDateDeSortie()));
                    numero.setText(String.valueOf(newValue.getNumeroEpisode()));
                    cout.setText(String.valueOf(newValue.getCout()));
                    recette.setText(String.valueOf(newValue.getRecette()));
                }
            });


            ObservableList<Film> items = FXCollections.observableArrayList(arrayList);
            listView.setItems(items);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleSubmitButtonAction(ActionEvent actionEvent) {
        try {
            DAOFilm daoFilm = new DAOFilm();

            Film film = new Film(
                    selectedFilm.getId(),
                    titre.getText(),
                    Integer.parseInt(date.getText()),
                    Integer.parseInt(numero.getText()),
                    Integer.parseInt(cout.getText()),
                    Integer.parseInt(recette.getText())
            );

            text.setText(daoFilm.update(film) ? "The film was successfully updated." : "The program could not update the Film.");

            Main.scene.setActivePane(
                    FXMLLoader.load(getClass().getResource("/view/filmlist.fxml"))
            );
        }
        catch (NumberFormatException | SQLException | ClassNotFoundException | IOException e) {
            e.printStackTrace();

            text.setText("The program could not update the Film.");
        }
    }

    public void handleLinkLogin(ActionEvent actionEvent) {
        try {
            Main.scene.setActivePane(
                    FXMLLoader.load(getClass().getResource("/view/index.fxml"))
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleLinkCreateFilm(ActionEvent actionEvent) {
        try {
            Main.scene.setActivePane(
                    FXMLLoader.load(getClass().getResource("/view/createfilm.fxml"))
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleLinkUploadFile(ActionEvent actionEvent) {
        try {
            Main.scene.setActivePane(
                    FXMLLoader.load(getClass().getResource("/view/fileupload.fxml"))
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
