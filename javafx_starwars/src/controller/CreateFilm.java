package controller;

import dao.DAOFilm;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import model.Film;
import sample.Main;

import java.io.IOException;
import java.sql.SQLException;

public class CreateFilm {

    public PasswordField passwordField;
    public TextField id;
    public TextField titre;
    public TextField date;
    public TextField numero;
    public TextField cout;
    public TextField recette;
    public Text text;

    public void handleSubmitButtonAction(ActionEvent actionEvent) {
        try {
            DAOFilm daoFilm = new DAOFilm();

            Film film = new Film(
                    Integer.parseInt(id.getText()),
                    titre.getText(),
                    Integer.parseInt(date.getText()),
                    Integer.parseInt(numero.getText()),
                    Integer.parseInt(cout.getText()),
                    Integer.parseInt(recette.getText())
            );

            text.setText(daoFilm.ajouter(film) ? "The film was successfully added." : "The program could not add the Film.");
        }
        catch (NumberFormatException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();

            text.setText("The program could not add the Film.");
        }
    }

    public void handleLinkViewAllFilms(ActionEvent actionEvent) {
        try {
            Main.scene.setActivePane(
                    FXMLLoader.load(getClass().getResource("/view/filmlist.fxml"))
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
