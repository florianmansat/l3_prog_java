module javafx.starwars {
    requires java.sql;
    requires mysql.connector.java;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    requires tomcat.servlet.api;
    requires java.desktop;

    exports controller;
    exports model;

    opens sample;
    opens controller;
}
