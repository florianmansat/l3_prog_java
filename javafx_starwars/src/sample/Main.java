package sample;

import handler.SceneHandler;
import handler.SessionHandler;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.stage.Screen;
import javafx.stage.Stage;


public class Main extends Application {
    public static SessionHandler session;
    public static SceneHandler scene;
    public static Stage primaryStage;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Main.primaryStage = primaryStage;
        Rectangle2D screenBounds = Screen.getPrimary().getBounds();
        Parent root = FXMLLoader.load(getClass().getResource("/view/fileupload.fxml"));

        session = new SessionHandler();
        scene = new SceneHandler(root, screenBounds.getWidth() * 0.6, screenBounds.getHeight() * 0.6);

        primaryStage.setTitle("Projet Star Wars");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
