package handler;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Paint;

import java.util.MissingResourceException;

public class SceneHandler extends Scene {

    private Parent activePane;
    private Parent oldPane;

    public SceneHandler(Parent parent) {
        super(parent);
        this.setActivePane(parent);
    }

    public SceneHandler(Parent parent, double v, double v1) {
        super(parent, v, v1);
        this.setActivePane(parent);
    }

    public SceneHandler(Parent parent, Paint paint) {
        super(parent, paint);
        this.setActivePane(parent);
    }

    public SceneHandler(Parent parent, double v, double v1, Paint paint) {
        super(parent, v, v1, paint);
        this.setActivePane(parent);
    }

    public SceneHandler(Parent parent, double v, double v1, boolean b) {
        super(parent, v, v1, b);
        this.setActivePane(parent);
    }

    public SceneHandler(Parent parent, double v, double v1, boolean b, SceneAntialiasing sceneAntialiasing) {
        super(parent, v, v1, b, sceneAntialiasing);
        this.setActivePane(parent);
    }



    public void swapOldToNewScene() throws MissingResourceException {
        if (this.oldPane != null) {
            this.setActivePane(this.getOldPane());
        }
        else {
            throw new MissingResourceException("oldPane n'est pas defini.", "MainScene", "oldPane");
        }
    }

    public Parent getActivePane() {
        return activePane;
    }

    public void setActivePane(Parent activePane) {
        this.setOldPane(this.activePane);
        this.activePane = activePane;
        this.setRoot(activePane);

        System.out.println("--------------------------------------------------------------");
        System.out.println("activePane = " + this.activePane);
        System.out.println("oldPane = " + this.oldPane);
    }

    public Parent getOldPane() {
        return oldPane;
    }

    private void setOldPane(Parent oldPane) {
        this.oldPane = oldPane;
    }
}

