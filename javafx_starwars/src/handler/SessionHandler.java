package handler;

import dao.DAOAcces;
import dao.DAOUser;
import model.Acces;

public class SessionHandler {
    public Acces utilisateur;

    public SessionHandler() {}

    public boolean login(String ndc, String mdp) {
        try {
            DAOUser daoUser = new DAOUser();

            this.utilisateur = daoUser.login(ndc, mdp);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return utilisateur != null;
    }
}
