package dao;

import model.Acces;
import model.Acteur;
import model.Film;

import java.sql.*;

public class DAOUser {
    /**
     * The Connect.
     */
    public Connection connect;

    /**
     * Instantiates a new Dao film.
     *
     * @throws SQLException           the sql exception
     * @throws ClassNotFoundException the class not found exception
     */
    public DAOUser() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        this.connect = DriverManager.getConnection("jdbc:mysql://localhost/bdd_exos_java?user=hearthstone&password=1234&useSSL=false"); // localhost
    }

    public Acces login(String login, String password) {
        Acces acces = null;

        try {
            PreparedStatement statement = this.connect.prepareStatement("SELECT * FROM acces WHERE login = ? AND password = ?", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet result = statement.executeQuery();

            if(result.first()) {
                acces = new Acces(
                        result.getInt("id"),
                        result.getString("prenom"),
                        result.getString("login"),
                        result.getString("password"),
                        result.getString("statut"),
                        result.getInt("age")
                );
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return acces;
    }

    public boolean register(Acces acces) {
        try {
            PreparedStatement statement = this.connect.prepareStatement("INSERT INTO acces VALUES (?, ?, ?, ?, ?, ?)");
            statement.setInt(1, acces.getId());
            statement.setString(2, acces.getPrenom());
            statement.setString(3, acces.getLogin());
            statement.setString(4, acces.getPassword());
            statement.setString(5, acces.getStatut());
            statement.setInt(6, acces.getAge());

            int affectedRows = statement.executeUpdate();

            return affectedRows != 0;
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }
}
