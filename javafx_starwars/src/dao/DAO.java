package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The type Dao.
 *
 * @param <T> the type parameter
 */
public abstract class DAO<T>
{
    /**
     * The Connect.
     */
    public Connection connect;

    /**
     * Instantiates a new Dao.
     *
     * @throws SQLException           the sql exception
     * @throws ClassNotFoundException the class not found exception
     */
    public DAO() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        this.connect = DriverManager.getConnection("jdbc:mysql://localhost/bdd_exos_java?user=hearthstone&password=1234&useSSL=false"); // localhost
    }

    /**
     * Instantiates a new Dao.
     *
     * @param connect the connect
     * @throws SQLException           the sql exception
     * @throws ClassNotFoundException the class not found exception
     */
    public DAO(Connection connect) throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        this.connect = connect;
    }

    /**
     * Gets connect.
     *
     * @return the connect
     */
    public Connection getConnect() {
        return connect;
    }

    /**
     * Create boolean.
     *
     * @param obj the obj
     * @return the boolean
     */
    public abstract boolean create(T obj);

    /**
     * Delete boolean.
     *
     * @param obj the obj
     * @return the boolean
     */
    public abstract boolean delete(T obj);

    /**
     * Update boolean.
     *
     * @param obj the obj
     * @return the boolean
     */
    public abstract boolean update(T obj);

    /**
     * Find t.
     *
     * @param id the id
     * @return the t
     */
    public abstract T find(int id);

    /**
     * Find all array list.
     *
     * @return the array list
     */
    public abstract ArrayList<T> findAll();
}
