package dao;

import model.Acteur;
import model.Film;

import java.sql.*;
import java.util.ArrayList;

/**
 * The type Dao acteur.
 */
public class DAOActeur {
    /**
     * The Connect.
     */
    public Connection connect;

    /**
     * Instantiates a new Dao acteur.
     *
     * @throws SQLException           the sql exception
     * @throws ClassNotFoundException the class not found exception
     */
    public DAOActeur() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        this.connect = DriverManager.getConnection("jdbc:mysql://localhost/bdd_exos_java?user=hearthstone&password=1234&useSSL=false"); // localhost
    }

    /**
     * Lister array list.
     *
     * @return the array list
     */
    public ArrayList<Acteur> Lister() {
        ArrayList<Acteur> arrayList = new ArrayList<>();

        try {
            Statement statement = this.connect.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM acteur");

            while(result.next()) {
                arrayList.add(
                        new Acteur(
                                result.getInt("id"),
                                result.getString("nom"),
                                result.getString("prenom")
                        )
                );
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return arrayList;
    }

    /**
     * Lister par film array list.
     *
     * @param film the film
     * @return the array list
     */
    public ArrayList<Acteur> ListerParFilm(Film film) {
        ArrayList<Acteur> arrayList = new ArrayList<>();

        try {
            PreparedStatement statement = this.connect.prepareStatement("SELECT * FROM acteur WHERE filmId = ?");
            statement.setInt(1, film.getId());

            ResultSet result = statement.executeQuery();

            while(result.next()) {
                arrayList.add(
                        new Acteur(
                                result.getInt("id"),
                                result.getString("nom"),
                                result.getString("prenom")
                        )
                );
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return arrayList;
    }

    /**
     * Ajouter.
     *
     * @param acteur the acteur
     * @param film   the film
     */
    public void ajouter(Acteur acteur, Film film) {
        try {
            PreparedStatement statement = this.connect.prepareStatement("INSERT INTO acteur VALUES (?, ?, ?, ?)");
            statement.setInt(1, acteur.getId());
            statement.setInt(2, film.getId());
            statement.setString(3, acteur.getNom());
            statement.setString(4, acteur.getPrenom());

            int affectedRows = statement.executeUpdate();
            System.out.println(affectedRows);
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Supprimer.
     *
     * @param acteur the acteur
     */
    public void supprimer(Acteur acteur) {
        try {
            PreparedStatement statement = this.connect.prepareStatement("DELETE FROM acteur WHERE id = ?");
            statement.setInt(1, acteur.getId());

            int affectedRows = statement.executeUpdate();
            System.out.println(affectedRows);
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Fermer la connection.
     *
     * @throws SQLException the sql exception
     */
    public void fermerLaConnection() throws SQLException {
        this.connect.close();
    }
}
