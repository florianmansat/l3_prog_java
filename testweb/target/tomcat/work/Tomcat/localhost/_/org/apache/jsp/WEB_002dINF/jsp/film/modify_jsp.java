/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.37
 * Generated at: 2021-04-28 10:27:23 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.WEB_002dINF.jsp.film;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import model.*;

public final class modify_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");

            if (request.getAttribute("film") != null) {
                Film film = (Film) request.getAttribute("film");
        
      out.write("\r\n");
      out.write("        <form action='updateFilm' method='post'>\r\n");
      out.write("            <input type='hidden' name='idFilm' id='idFilm' value='");
      out.print(film.getId());
      out.write("'>\r\n");
      out.write("            <div>\r\n");
      out.write("                <label for='name'>Titre :</label>\r\n");
      out.write("                <input type='text' name='titre' id='titre' value='");
      out.print(film.getTitre());
      out.write("' required>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div>\r\n");
      out.write("                <label for='name'>Date de sortie :</label>\r\n");
      out.write("                <input type='number' name='dateDeSortie' id='dateDeSortie' value='");
      out.print(film.getDateDeSortie());
      out.write("' required>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div>\r\n");
      out.write("                <label for='name'>Numéro de l'épisode :</label>\r\n");
      out.write("                <input type='number' name='numero' id='numero' value='");
      out.print(film.getNumeroEpisode());
      out.write("' required>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div>\r\n");
      out.write("                <label for='name'>Coût :</label>\r\n");
      out.write("                <input type='number' name='cout' id='cout' value='");
      out.print(film.getCout());
      out.write("' required>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div>\r\n");
      out.write("                <label for='name'>Recette :</label>\r\n");
      out.write("                <input type='number' name='recette' id='recette' value='");
      out.print(film.getRecette());
      out.write("' required>\r\n");
      out.write("            </div>\r\n");
      out.write("            <button type='submit'>Enregistrer</button>\r\n");
      out.write("        </form>\r\n");
      out.write("        ");

            }
            else {
        
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <h1>No Film record found.</h1>\r\n");
      out.write("\r\n");
      out.write("        ");
 } 
      out.write("\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
